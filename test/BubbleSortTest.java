import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;


public class BubbleSortTest {

	ISort s = new BubbleSort();
	@Test
	public void testSort() {
		//fail("Not yet implemented");
	
		
		/* eth */
		int [] t10 =new int[] {1, 2, 3, 4, 5, 6, 7};
		
		int [] t11 =new int[] {1, 2, 3, 4, 5, 6, 7};
		s.Sort(t11);
		assertTrue(Arrays.equals(t11, t10));
		
		int [] t12 =new int[] {1, 2, 4, 3, 6, 5, 7};
		s.Sort(t12);
		assertTrue(Arrays.equals(t12, t10));
		
		int [] t13 =new int[] {7, 6, 5, 4, 3, 2, 1};
		s.Sort(t13);
		assertTrue(Arrays.equals(t13, t10));
		
		int [] t14 =new int[] {1, 3, 7, 2, 6, 5, 4};
		s.Sort(t14);
		assertTrue(Arrays.equals(t14, t10));
			
	}
	
	@Test
	public void testRepeating() {
		
		int [] t20 = new int [] {1, 1, 2, 2, 4, 4, 4};
		
		
		
		int [] t21 = new int [] {2, 2, 1, 1, 4, 4, 4};
		
		s.Sort(t21);
		assertTrue(Arrays.equals(t21, t20));
		
		int [] t30 = new int [] {0, 120, 120, 120, 120, 120};
		int [] t31 = new int [] {120, 120, 120, 120, 120, 0};
		s.Sort(t31);
		assertTrue(Arrays.equals(t31, t30));
		
		
	}
	
	@Test 
	public void testNegatives ()
	{
		int [] t40 = new int [] {-30, -20,  0,  12, 44, 50};
		
		
		int [] t41 = new int [] {-20, -30, 12, 44, 50, 0};
		s.Sort(t41);
		assertTrue(Arrays.equals(t40, t41));
	}
	
	@Test 
	public void testMore ()
	{
		int [] t50 = new int [] {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1};
		
		int [] t51 = new int [] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 1, 0};
		s.Sort (t51);
		assertTrue (Arrays.equals(t50, t51));
	
	}
	
	@Test
	public void testBig()
	{
		int [] t60 = new int [] { -2147483648, 0, 0, 12, 2147483647 };
		
		int [] t61 = new int [] { 2147483647, 0, -2147483648, 12, 0  };
		s.Sort(t61);
		assertTrue (Arrays.equals(t61, t60));
	}
}
