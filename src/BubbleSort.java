
public class BubbleSort implements ISort {

	@Override
	public void Sort (int[] array)
	{
		for (int i = 0; i < array.length - 1; i++)
		for (int j = 0; j < array.length - 1 - i; j++)
			if (array[j] > array[j+1]) 
			{
				swap (array, j, j+1);
			}
	}
	private void swap (int [] array, int i,int j)
	{
		int buffer = array[i];
		array[i] = array[j];
		array[j] = buffer;
	}
	
}
