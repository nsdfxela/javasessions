
public class MergeSort implements ISort {

	@Override
	public void Sort (int[] array){
		mergeSort (array, 0, array.length-1);
	}
	
	private void mergeSort (int [] array, int l, int r){
		
		if (r-l == 0) return;
		int m = l + (r-l) / 2;
		
		mergeSort (array, l, m);
		mergeSort (array, m+1, r);
		merge(array, l, m, r);
		
		//mergeSort (array.);
	}
	
	private void merge(int [] array, int l, int m, int r){
		int [] bufferArray = new int [r-l+1];
		
		int n1=l, n2=m+1;
		int b=0;
		while (n1 <= m && n2 <= r)
		{
			if (array[n1] < array[n2])
			{
			 bufferArray[b++] = array[n1];
			 n1++;
			}
			if (array[n1] >= array[n2])
			{
			 bufferArray[b++] = array[n2];
			 n2++;
			}
		}
		
		while (n1 < m+1)
		{
			bufferArray[b++] = array[n1++];
		} 
		while (n2 < r+1)
		{
			bufferArray[b++] = array[n2++];
		} 
		
		for (int i = 0; i < bufferArray.length; i++){
			array[l+i] = bufferArray[i];
		}
			
	}
	
}

