
public class QuickSort implements ISort {

	@Override
	public void Sort(int[] array) {
			
		sortArrayBounds (array, 0, array.length-1);
	}
	private void sortArrayBounds (int [] array, int l, int r){ 
		// ������� ��� �������� �������
			int m = (r-l) / 2 + l ;
			
			int i = l;
			int j = r;
			
			while (i < j)
			{ 
				//���� ����� �� ��� ��� ���� �� ������ ������� ��� ������ �������
				for(; array[i]<array[m]; i++);
				//���� ������ �� ��� ��� ���� �� ������ ������� ��� ������ �������
				for(; array[j]>array[m]; j--);	
		 
				//������ �������
				if ((i < j) ) {		
						swap(array, i, j);
						
						//���� ������� ������� �����, ��������� �� ����� ������
							if (i == m )
							{m = j; i++;}
							else if (j == m )
							{m = i; j--;}
							else {i++; j--;}
					}
			}
			
			//��������� �����
			if (m-1 >= l) sortArrayBounds (array, l, m-1);
			//��������� ������
			if (m+1 <= r) sortArrayBounds (array, m+1, r);
						
	}
	private void swap(int [] a, int i1, int i2)
	{
		int buffer = a[i1];
		a[i1] = a[i2];
		a[i2] = buffer;
	}
}
